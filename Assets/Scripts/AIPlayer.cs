using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPlayer 
{
    [SerializeField] private int maxSearchDepth = 10;
    [SerializeField] private int winScore = 10;

    private int id;

    public AIPlayer(int _id)
    {
        this.id = _id;
    }

    public int MakeSillyMove(List<int> _board)
    {
        // simply fill in the first untaken
        int position = Random.Range(0, 9); ;
        while (_board[position] != 0)
        {
            position = Random.Range(0, 9); ;
        }
        return position;
    }

    // 0 - not taken, 1 - X taken, 2 - O taken
    public int MakeMove(List<int> _board)
    {
        // MinMax find best move
        int bestValue = -10000;
        int movePostition = -1;

        for (int i = 0; i < _board.Count; i++)
        {
            if (_board[i] == 0)
            {
                _board[i] = this.id;
                int value = Minimax(_board, 0, maxSearchDepth, false);
                _board[i] = 0;

                if (value > bestValue)
                {
                    bestValue = value;
                    movePostition = i;
                }
            }
        }

        return movePostition;
    }

    private int Minimax(List<int> _board, int _currentDepth, int _maxDepth, bool isMax)
    {
        int result = GamePlayManager.Instance.JudgeResult(_board);
        if (result == 1 || result == 2)
        {
            return (result == this.id ? winScore : -1 * winScore);
        }

        if (_currentDepth == _maxDepth || result == 3)
        {
            return EvaluateBoard();
        }

        if (isMax)
        {
            int best = -10000;
            for (int i = 0; i < _board.Count; i++)
            {
                if (_board[i] == 0)
                {
                    _board[i] = this.id;
                    best = Mathf.Max(best, Minimax(_board, _currentDepth + 1, _maxDepth, false));
                    _board[i] = 0;
                }
            }
            return best;
        }
        else
        {
            int best = 10000;
            for (int i = 0; i < _board.Count; i++)
            {
                if (_board[i] == 0)
                {
                    _board[i] = 3 - this.id;
                    best = Mathf.Min(best, Minimax(_board, _currentDepth + 1, _maxDepth, true));
                    _board[i] = 0;
                }
            }
            return best;
        }

    }

    // TODO: Evaluate the current situation for this player, NOT WIN YET
    private int EvaluateBoard()
    {
        return 0;
    }
}
