using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayManager : Singleton<GamePlayManager>
{

    private AIPlayer Player1;                       // Player 1 for X
    private AIPlayer Player2;                       // Player 2 for O
    private bool isPlayer1Turn = false;
    private int roundNumber = 0;
    private int player1WinCount = 0;
    private int player2WinCount = 0;
    private int move = 0;
    private int drawCount = 0;
    private List<int> board = new List<int>();      // 0 - not taken, 1 - X taken, 2 - O taken
    [SerializeField] private Text resultTxt;

    public float interval = 0.5f;
    public List<GameObject> boardDisplay;


    // Start is called before the first frame update
    void Start()
    {
        Player1 = new AIPlayer(1);
        Player2 = new AIPlayer(2);

        isPlayer1Turn = Random.Range(0, 2) == 0 ? true : false;

        StartCoroutine(Gameloop());
    }

    IEnumerator Gameloop()
    {
        while(true)
        {
            yield return StartCoroutine(RoundStart());
            yield return StartCoroutine(RoundGoing());
            resultTxt.text = "Round: " + roundNumber + " Draw: " + drawCount + " X Won: " + player1WinCount + " O Won: " + player2WinCount;
            isPlayer1Turn = Random.Range(0, 2) == 0 ? true : false;
        }
    }

    IEnumerator RoundStart()
    {
        move = 0;
        board.Clear();
        for (int i = 0; i < boardDisplay.Count; i++)
        {
            boardDisplay[i].GetComponent<Text>().text = "";
            board.Add(0);
        }
        roundNumber++;
        yield return new WaitForSeconds(interval);
    }

    IEnumerator RoundGoing()
    {
        int result = JudgeResult(this.board);
        // 0 - continue, 1 - Player1 win, 2 - Player2 win, 3 - Draw
        while (result == 0)
        {
            if (isPlayer1Turn)
            {
                if (move == 0)
                {
                    this.board[Player1.MakeSillyMove(board)] = 1;
                }
                else
                {
                    this.board[Player1.MakeMove(board)] = 1;
                }
                isPlayer1Turn = false;
            }
            else
            {
                if (move == 0)
                {
                    this.board[Player1.MakeSillyMove(board)] = 2;
                }
                else
                {
                    this.board[Player2.MakeMove(board)] = 2;
                }
                isPlayer1Turn = true;
            }
            UpdateBoard();
            move++;
            result = JudgeResult(this.board);
            yield return new WaitForSeconds(interval);
        }
        switch (result)
        {
            case 1:
                player1WinCount++;
                Debug.Log("Player 1 Win: " + player1WinCount);
                break;
            case 2:
                player2WinCount++;
                Debug.Log("Player 2 Win: " + player2WinCount);
                break;
            case 3:
                drawCount++;
                Debug.Log("Draw: " + drawCount);
                break;
            default:
                break;
        }
        yield return new WaitForSeconds(interval);
    }

    // 0 - continue, 1 - Player1 win, 2 - Player2 win, 3 - Draw
    public int JudgeResult(List<int> board)
    {
        List<int> player1Places = new List<int>();
        List<int> player2Places = new List<int>();

        for (int i = 0; i < board.Count; i++)
        {
            if (board[i] == 1)
            {
                player1Places.Add(i);
            }
            if (board[i] == 2)
            {
                player2Places.Add(i);
            }
        }

        if (ContainsWinPattern(player1Places))
        {
            return 1;
        }

        if (ContainsWinPattern(player2Places))
        {
            return 2;
        }

        if (IsBoardFull(board))
        {
            return 3;
        }
        
        return 0;
    }

    private bool IsBoardFull(List<int> board)
    {
        for (int i = 0; i < board.Count; i++)
        {
            if (board[i] == 0)
            {
                return false;
            }
        }
        return true;
    }

    private bool ContainsWinPattern(List<int> places)
    {
        if (places.Contains(0) && places.Contains(1) && places.Contains(2) ||
            places.Contains(3) && places.Contains(4) && places.Contains(5) ||
            places.Contains(6) && places.Contains(7) && places.Contains(8) ||
            places.Contains(0) && places.Contains(3) && places.Contains(6) ||
            places.Contains(1) && places.Contains(4) && places.Contains(7) ||
            places.Contains(2) && places.Contains(5) && places.Contains(8) ||
            places.Contains(0) && places.Contains(4) && places.Contains(8) ||
            places.Contains(2) && places.Contains(4) && places.Contains(6))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // 0 - not taken, 1 - X taken, 2 - O taken
    private void UpdateBoard()
    {
        for (int i = 0; i < board.Count; i++)
        {
            if(board[i] == 1)
            {
                boardDisplay[i].GetComponent<Text>().text = "X";
            }
            if (board[i] == 2)
            {
                boardDisplay[i].GetComponent<Text>().text = "O";
            }
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }
}
